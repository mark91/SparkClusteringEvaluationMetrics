/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.spark.ml.evaluation

import org.apache.spark.SparkContext
import org.apache.spark.annotation.{Experimental, Since}
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.ml.evaluation.CosineSilhouetteEvaluator.ClusterStats
import org.apache.spark.ml.linalg.{DenseVector, Vector, VectorElementWiseSum, VectorUDT}
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.ml.param.shared.{HasFeaturesCol, HasPredictionCol}
import org.apache.spark.ml.util.{Identifiable, SchemaUtils}
import org.apache.spark.sql.{DataFrame, Dataset}
import org.apache.spark.sql.functions.{avg, col, count}
import org.apache.spark.sql.types.IntegerType

/**
 *  :: Experimental ::
 * Evaluator for clustering results which returns the Silhouette
 * metric using the cosine distance.
 * The implementation follows the proposal explained
 * <a href="https://drive.google.com/file/d/0B0Hyo%5f%5fbG%5f3fdkNvSVNYX2E3ZU0/view">
 *   in this document</a>.
 *
 */
@Since("2.3.0")
@Experimental
class CosineSilhouetteEvaluator(override val uid: String)
  extends Evaluator with HasPredictionCol with HasFeaturesCol {
  @Since("2.3.0")
  def this() = this(Identifiable.randomUID("SquaredEuclideanSilhouette"))

  private[this] var kryoRegistrationPerformed: Boolean = false

  @Since("2.3.0")
  override def copy(pMap: ParamMap): Evaluator = this.defaultCopy(pMap)

  @Since("2.3.0")
  override def isLargerBetter: Boolean = true

  /** @group setParam */
  @Since("2.3.0")
  def setPredictionCol(value: String): this.type = set(predictionCol, value)

  /** @group setParam */
  @Since("2.3.0")
  def setFeaturesCol(value: String): this.type = set(featuresCol, value)

  @Since("2.3.0")
  override def evaluate(dataset: Dataset[_]): Double = {
    // sanity checks
    SchemaUtils.checkColumnType(dataset.schema, $(featuresCol), new VectorUDT)
    SchemaUtils.checkColumnType(dataset.schema, $(predictionCol), IntegerType)

    registerKryoClasses(dataset.sparkSession.sparkContext)

    val computeCsi = dataset.sparkSession.udf.register("computeCsi",
      CosineSilhouetteEvaluator.computeCsi _ )
    val dfWithCsi = dataset.withColumn("csi", computeCsi(col($(featuresCol))))

    // compute aggregate values for clusters
    // needed by the algorithm
    val clustersAggregateValues = CosineSilhouetteEvaluator
      .computeOmegaAndCount(dfWithCsi, $(predictionCol), $(featuresCol))

    val clustersMap = clustersAggregateValues.collect().map(row => {
      row.getAs[Int]($(predictionCol)) ->
        ClusterStats(row.getAs[Vector]("omega"), row.getAs[Long]("count"))
    }).toMap

    val broadcastedClustersMap = dataset.sparkSession.sparkContext.broadcast(clustersMap)

    val computeSilhouette = dataset.sparkSession.udf.register("computeSilhouette",
      CosineSilhouetteEvaluator
        .computeCosineSilhouetteCoefficient(broadcastedClustersMap, _: Vector, _: Int, _: Vector)
    )

    val squaredSilhouetteDF = dfWithCsi
      .withColumn("silhouetteCoefficient",
        computeSilhouette(col($(featuresCol)), col($(predictionCol)), col("csi"))
      )
      .agg(avg(col("silhouetteCoefficient")))

    squaredSilhouetteDF.collect()(0).getDouble(0)
  }

  /**
    * This method registers the class
    * [[org.apache.spark.ml.evaluation.CosineSilhouetteEvaluator.ClusterStats]]
    * for kryo serialization.
    * @param sc `SparkContext` to be used
    */
  private[this] def registerKryoClasses(sc: SparkContext): Unit = {
    if (! kryoRegistrationPerformed) {
      sc.getConf.registerKryoClasses(
        Array(
          classOf[CosineSilhouetteEvaluator.ClusterStats]
        )
      )
      kryoRegistrationPerformed = true
    }
  }


}

private[this] object CosineSilhouetteEvaluator {

  case class ClusterStats(omega: Vector, count: Long)

  def computeOmegaAndCount(
      df: DataFrame,
      predictionCol: String,
      featuresCol: String): DataFrame = {
    val omegaUdaf = new VectorElementWiseSum()
    df.groupBy(predictionCol)
      .agg(
        count("*").alias("count"),
        omegaUdaf(col("csi")).alias("omega")
      )
  }

  def computeCsi(vector: Vector): Vector = {
    var sum: Double = 0.0
    vector.foreachActive( (_, i) => {
      sum += i * i
    })
    val norm = math.sqrt(sum)
    new DenseVector(vector.toArray.map( _ / norm ))
  }

  def computeCosineSilhouetteCoefficient(
      broadcastedClustersMap: Broadcast[Map[Int, ClusterStats]],
      vector: Vector,
      clusterId: Int,
      csi: Vector): Double = {

    def compute(point: Vector, csi: Vector, clusterStats: ClusterStats): Double = {
      var omegaMultiplyCsiSum: Double = 0.0
      csi.foreachActive( (i, iCsi) => {
        omegaMultiplyCsiSum += clusterStats.omega(i) * iCsi
      })

      1 - omegaMultiplyCsiSum / clusterStats.count
    }

    var minOther = Double.MaxValue
    for(c <- broadcastedClustersMap.value.keySet) {
      if (c != clusterId) {
        val sil = compute(vector, csi, broadcastedClustersMap.value(c))
        if(sil < minOther) {
          minOther = sil
        }
      }
    }
    val clusterCurrentPoint = broadcastedClustersMap.value(clusterId)
    // adjustment for excluding the node itself from
    // the computation of the average dissimilarity
    val clusterSil = if (clusterCurrentPoint.count == 1) {
      0
    } else {
      compute(csi, vector, clusterCurrentPoint) * clusterCurrentPoint.count /
        (clusterCurrentPoint.count - 1)
    }

    var silhouetteCoeff = 0.0
    if (clusterSil < minOther) {
      silhouetteCoeff = 1 - (clusterSil / minOther)
    } else {
      if (clusterSil > minOther) {
        silhouetteCoeff = (minOther / clusterSil) - 1
      }
    }
    silhouetteCoeff

  }

}
